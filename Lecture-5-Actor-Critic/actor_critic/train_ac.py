import numpy as np
import tensorflow as tf
import gym
import roboschool
import scipy.signal
import os
import time
import inspect

import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', '..'))
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))

import logz

# ============================================================================================#
# Utilities
#============================================================================================#
def normalize(data):
    n_data = (data - np.mean(data)) / (np.std(data) + 1e-8)
    return n_data

def build_mlp(
        input_placeholder,
        output_size,
        scope,
        n_layers=2,
        size=64,
        activation=tf.nn.relu,
        output_activation=None
        ):
    #========================================================================================#
    # Build the network policy
    #========================================================================================#
    with tf.variable_scope(scope):
        out = input_placeholder
        for l in range(n_layers):
            out = tf.layers.dense(inputs=out, units=size, activation=activation)
        out = tf.layers.dense(inputs=out, units=output_size, activation=output_activation)
        return out

#============================================================================================#
# Actor Critic
#============================================================================================#

def train_AC(exp_name='',
             env_name='CartPole-v0',
             n_iter=100,
             gamma=1.0,
             min_timesteps_per_batch=1000,
             actor_learning_rate=5e-3,
             critic_learning_rate=5e-3,
             animate=True,
             logdir=None,
             v_reward_to_go=False,
             seed=0,
             # network arguments
             n_layers=1,
             size=32
             ):

    # Configure output directory for logging
    logz.configure_output_dir(logdir)

    # Log experimental parameters
    args = inspect.getargspec(train_AC)[0]
    locals_ = locals()
    params = {k: locals_[k] if k in locals_ else None for k in args}
    logz.save_params(params)

    # Set random seeds
    tf.reset_default_graph()
    tf.set_random_seed(seed)
    np.random.seed(seed)

    # Make the gym environment
    env = gym.make(env_name)
    
    # Is this env continuous, or discrete?
    discrete = isinstance(env.action_space, gym.spaces.Discrete)

    # Maximum length for episodes
    max_episode_steps = env.spec.max_episode_steps

    #========================================================================================#
    # Notes on notation:
    #
    # Tensorflow operations have the prefix ops_, to distinguish them from the numerical values
    # that are computed later in the function
    #
    # Prefixes and suffixes:
    # ob - observation
    # ac - action
    # _no - this tensor should have shape (batch size /n/, observation dim)
    # _na - this tensor should have shape (batch size /n/, action dim)
    # _n  - this tensor should have shape (batch size /n/)
    # _nac - this tensor should have shape _n (discrete action) or _na (continuous action)
    #
    # Note: batch size /n/ is defined at runtime, and until then, the shape for that axis
    # is None
    #========================================================================================#

    # Observation and action sizes
    ob_dim = env.observation_space.shape[0]
    ac_dim = env.action_space.n if discrete else env.action_space.shape[0]

    #========================================================================================#
    # Placeholders
    #========================================================================================#

    # Observations are input for everything: sampling actions, baselines, policy gradients
    ops_ob_no = tf.placeholder(shape=[None, ob_dim], name="ob", dtype=tf.float32)

    # Actions are input when computing policy gradient updates
    if discrete:
        ops_nac = tf.placeholder(shape=[None], name="ac", dtype=tf.int32)
    else:
        ops_nac = tf.placeholder(shape=[None, ac_dim], name="ac", dtype=tf.float32)

    # Advantages are input when computing policy gradient updates
    ops_adv_n = tf.placeholder(shape=[None], name="adv", dtype=tf.float32)
    # ========================================================================================#

    #========================================================================================#
    # Define symbolic variable (Operators in Tensorflow)
    #========================================================================================#
    if discrete:
        # Compute stochastic policy over discrete actions
        ops_logits_na = build_mlp(ops_ob_no, ac_dim, "actor", n_layers=n_layers, size=size)

        # Sample an action from the stochastic policy
        ops_sampled_nac = tf.multinomial(ops_logits_na, 1)
        ops_sampled_nac = tf.reshape(ops_sampled_nac, [-1])

        # Likelihood of chosen action
        ops_logprob_n = -tf.nn.sparse_softmax_cross_entropy_with_logits(labels=ops_nac, logits=ops_logits_na)

    else:
        # Compute Gaussian stochastic policy over continuous actions.
        # The mean is a function of observations, while the variance is not.
        ops_mean_na = build_mlp(ops_ob_no, ac_dim, "actor", n_layers=n_layers, size=size)
        ops_logstd = tf.Variable(tf.zeros([1, ac_dim]), name="actor/logstd", dtype=tf.float32)
        ops_std = tf.exp(ops_logstd)

        # Sample an action from the stochastic policy
        ops_sampled_z = tf.random_normal(tf.shape(ops_mean_na))
        ops_sampled_nac = ops_mean_na + ops_std * ops_sampled_z

        # Likelihood of chosen action
        ops_z = (ops_nac - ops_mean_na) / ops_std
        ops_logprob_n = -0.5 * tf.reduce_sum(tf.square(ops_z), axis=1)
    # ========================================================================================#

    #========================================================================================#
    # Actor update operation
    #========================================================================================#
    actor_loss = -tf.reduce_mean(ops_logprob_n * ops_adv_n)
    actor_update_op = tf.train.AdamOptimizer(actor_learning_rate).minimize(actor_loss)

    #========================================================================================#
    # Critic implementation
    #========================================================================================#
    V_predict = tf.squeeze(build_mlp(
                            ops_ob_no,
                            1,
                            "critic",
                            n_layers=n_layers,
                            size=size))
    ops_target_v = tf.placeholder(shape=[None], name="v_target", dtype=tf.float32)
    critic_loss = tf.nn.l2_loss(V_predict - ops_target_v)
    critic_update_op = tf.train.AdamOptimizer(critic_learning_rate).minimize(critic_loss)

    # ====================================================================================#
    # Computing Q-values (reward-to-go)
    # ====================================================================================#
    def reward_to_go(samples):
        q_n = []
        for sample in samples:
            q = 0
            q_path = []

            # Dynamic programming over reversed path
            for rew in reversed(sample["reward"]):
                q = rew + gamma * q
                q_path.append(q)
            q_path.reverse()
            q_n.extend(q_path)

        return q_n

    # ====================================================================================#
    # Collect data
    # ====================================================================================#
    def collect_data():
        # Collect data until we have enough data samples
        timesteps_this_batch = 0
        samples = []
        while True:
            # Simulate one episode and get a sample
            ob = env.reset()
            obs, acs, rewards, next_obs, terminals = [], [], [], [], []
            animate_this_episode=(len(samples)==0 and (itr % 10 == 0) and animate)
            steps = 0
            while True:
                if animate_this_episode:
                    env.render()
                obs.append(ob)
                ac = sess.run(ops_sampled_nac, feed_dict={ops_ob_no : [ob]})
                ac = ac[0]
                acs.append(ac)
                # Simulate one time step
                ob, rew, done, _ = env.step(ac)

                next_obs.append(ob)
                rewards.append(rew)
                steps += 1
                if done or steps > max_episode_steps:
                    terminals.append(1)
                    break
                else:
                    terminals.append(0)

            sample = {"observation": np.array(obs, dtype=np.float32),
                    "reward": np.array(rewards, dtype=np.float32),
                    "action": np.array(acs, dtype=np.float32),
                    "next_observation": np.array(next_obs, dtype=np.float32),
                    "terminal": np.array(terminals, dtype=np.float32)}

            samples.append(sample)
            timesteps_this_batch += len(sample["reward"])

            if timesteps_this_batch >= min_timesteps_per_batch:
                break
        return samples, timesteps_this_batch

    def get_adv():
        # ====================================================================================#
        # Critic implementation
        # ====================================================================================#
        if v_reward_to_go:
            q_n = reward_to_go(samples)
            v_n = sess.run(V_predict, feed_dict={ops_ob_no: ob_no})
            adv_n = q_n - v_n
        else:
            v_n = sess.run(V_predict, feed_dict={ops_ob_no: ob_no})
            v_n_plus_1 = sess.run(V_predict, feed_dict={ops_ob_no: next_ob_no})

            target_v = re_n + gamma * v_n_plus_1 * (1 - terminal_n)
            adv_n = target_v - v_n

        # ====================================================================================#
        # Advantage Normalization
        # ====================================================================================#
        adv_n = normalize(adv_n)

        if v_reward_to_go:
            return adv_n, q_n
        else:
            return adv_n, target_v
    #========================================================================================#
    # Tensorflow Engineering: Config, Session, Variable initialization
    #========================================================================================#
    sess = tf.Session()
    sess.__enter__() # equivalent to `with sess:`
    tf.global_variables_initializer().run() #pylint: disable=E1101

    #========================================================================================#
    # Training Loop
    #========================================================================================#

    total_timesteps = 0
    for itr in range(n_iter):
        print("********** Iteration %i ************"%itr)

        # Collect data until we have enough data samples
        samples, timesteps_this_batch = collect_data()

        total_timesteps += timesteps_this_batch

        # Build arrays for observation, action for the policy gradient update by concatenating 
        # across samples
        ob_no = np.concatenate([sample["observation"] for sample in samples])
        ac_nac = np.concatenate([sample["action"] for sample in samples])
        re_n = np.concatenate([path["reward"] for path in samples])
        next_ob_no = np.concatenate([path["next_observation"] for path in samples])
        terminal_n = np.concatenate([path["terminal"] for path in samples])

        adv_n, target_v = get_adv()
        #====================================================================================#
        # Performing the Actor Update
        #====================================================================================#
        [update_op, loss] = sess.run([actor_update_op, actor_loss], feed_dict={ops_ob_no : ob_no, ops_nac : ac_nac, ops_adv_n : adv_n})
        #====================================================================================#
        # Performing the Critic Update
        #====================================================================================#
        sess.run(critic_update_op, feed_dict={ops_ob_no: ob_no, ops_target_v: target_v})

        # Log diagnostics
        returns = [sample["reward"].sum() for sample in samples]
        ep_lengths = [len(sample["reward"]) for sample in samples]
        logz.log_tabular("Iteration", itr)
        logz.log_tabular("AverageReturn", np.mean(returns))
        logz.log_tabular("StdReturn", np.std(returns))
        logz.log_tabular("MaxReturn", np.max(returns))
        logz.log_tabular("MinReturn", np.min(returns))
        logz.log_tabular("EpLenMean", np.mean(ep_lengths))
        logz.log_tabular("EpLenStd", np.std(ep_lengths))
        logz.log_tabular("TimestepsThisBatch", timesteps_this_batch)
        logz.log_tabular("TimestepsSoFar", total_timesteps)
        logz.log_tabular("LossValue", loss)
        logz.dump_tabular()
        logz.pickle_tf_vars()

    sess.__exit__(None, None, None)

def main():

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--env_name', help='Environment ID', type=str, default='Pendulum-v0')
    parser.add_argument('--exp_name', type=str, default='vac')
    parser.add_argument('--n_experiments', '-e', type=int, default=1)
    parser.add_argument('--render', action='store_true', default=False)
    parser.add_argument('--seed', type=int, default=1)
    parser.add_argument('--discount', type=float, default=1.0)
    parser.add_argument('--n_iter', '-n', type=int, default=1000)
    parser.add_argument('--n_layers', '-l', type=int, default=2)
    parser.add_argument('--size', '-s', type=int, default=32)
    parser.add_argument('--batch_size', '-b', type=int, default=1000)

    parser.add_argument('--actor_learning_rate', '-alr', type=float, default=5e-3)
    parser.add_argument('--critic_learning_rate', '-clr', type=float, default=5e-3)
    parser.add_argument('--v_reward_to_go', '-rtg_v', action='store_true')

    args = parser.parse_args()

    logs_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'logs')
    logdir = os.path.join(logs_path, args.exp_name, args.env_name, time.strftime("%d-%m-%Y_%H-%M-%S"))

    if not(os.path.exists(logdir)):
        os.makedirs(logdir)

    for e in range(args.n_experiments):
        seed = args.seed + 10*e
        print('Running experiment with seed %d'%seed)
        train_AC(
            exp_name=args.exp_name,
            env_name=args.env_name,
            n_iter=args.n_iter,
            gamma=args.discount,
            min_timesteps_per_batch=args.batch_size,
            actor_learning_rate=args.actor_learning_rate,
            critic_learning_rate=args.critic_learning_rate,
            animate=args.render,
            logdir=os.path.join(logdir,'%d'%seed),
            v_reward_to_go=args.v_reward_to_go,
            seed=seed,
            n_layers=args.n_layers,
            size=args.size
            )

if __name__ == "__main__":
    main()
