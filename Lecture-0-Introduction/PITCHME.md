---?image=assets/img/conference.png&position=bottom&size=100% 100%
@snap[midpoint span-65]
@box[bg-blue text-white rounded](Lecture 0#Course Introduction)
@snapend

---
@snap[north-west]
**What this course will cover?**
@snapend

@snap[midpoint list-content-concise span-100]
@ul[list-bullets-circles]
- From supervised learning to decision making.
- Model-free algorithms: Q-learning, policy gradients, actor-critic.
- Advanced model learning and prediction.
- Transfer and multi-task learning, meta-learning.
- Open problems.
@ulend
@snapend

---
@snap[north-west]
**What you should know?**
@snapend

@snap[midpoint list-content-concise span-100]
@ul[list-bullets-circles]
- Machine Learning
- Python
- Tensorflow
@ulend
@snapend

---
**What is deep reinforcement learning, and why should we care?**

+++?image=assets/img/robot.png&position=right&size=auto 100%
@snap[west span-50]
@quote[Reinforcement Learning is a framework to build intelligence machines that can adapt with environments.]
@snapend

+++?image=assets/img/deep_learning_example.jpg&position=left&size=50% auto
@snap[east span-50]
@quote[Deep learnings helps us handle *unstructured* environments by allowing to build very large and powerful models that can deal with raw observations]
@snapend

+++?image=assets/img/behavior.png&position=center&size=80% auto
@snap[north span-100]
@quote[Reinforcement learning provides a formalism for behavior]
@snapend

+++?image=assets/img/drl.png&position=center&size=80% auto

---
**Deep reinforcement learning applications?**

+++
@snap[midpoint span-100]
![Video](https://www.youtube.com/embed/qhUvQiKec2U)
@snapend
@snap[south span-100]
@quote[Imitation learning: learn a policy using demonstrations from human]
@snapend

+++?image=assets/img/atari.gif&position=center&size=auto 60%
@snap[south span-100]
@quote[Known rules games such as Go game, Atari]
@snapend

+++?image=assets/img/humanoid.gif&position=left&size=50% auto
@snap[east span-50]
@quote[Learn simple skills with raw sensory inputs, given enough experience such as Robotics]
@snapend

+++?image=assets/img/trafficsmoothing.gif&position=center&size=auto 60%
@snap[south span-100]
@quote[Control the traffic which includes autonomuous cars and human-driven cars]
@snapend

+++
**, and so on ...**

---
@snap[north-west]
**Advanced topics**
@snapend

@snap[midpoint list-content-concise span-100]
@ul[list-bullets-circles]
- Model-basded Reinforcement Learning
- Inverse Reinforcement Learning
- Transfer Learning and Multi-Tasks learning
- Meta Learning
@ulend
@snapend

+++
**, and so on ...**

---
@snap[north-west]
**Outlines of the course**
@snapend

@snap[midpoint list-content-concise span-100]
@ul[list-bullets-circles]
- Lecture 1: Imitation Learning
- Lecture 2: Tensorflow
- Lecture 3: Reinforcement Learning
- Lecture 4: Policy gradient Algorithms
- Lecture 5: Actor-Critic Algorithms
- Lecture 6: Value-based Algorithms
- Lecture ...: Decided later (model-based, meta learning, inverse RL)
@ulend
@snapend

---
**Material**

+++
@snap[north-west]
**Implementation**
@snapend

```bash
git clone https://gitlab.com/agilesoda/rl_continuous.git
```
@[1](Clone from gitlabs)

+++
@snap[north-west]
**Presentation**
@snapend

```bash
git clone https://gitlab.com/tuyenple/rl_lectures.git
```
@[1](Clone from gitlabs)

---
**Thank you**