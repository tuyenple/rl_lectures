import argparse
import os
import gym
import pybullet_envs
import numpy as np
import pandas as pd
import pickle
try:
    import roboschool
except ImportError:
    roboschool = None
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.optimizers import RMSprop
from keras.utils import np_utils
import random

import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', '..'))
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))

from stable_baselines.common import set_global_seeds
from stable_baselines.common.vec_env import VecNormalize, VecFrameStack
from stable_baselines.results_plotter import load_results, ts2xy

from tested_environments.utils import ALGOS, create_test_env, get_latest_run_id, get_saved_hyperparams

def print_performance(path):
    x, y = ts2xy(load_results(path), 'timesteps')

    if len(x) > 0:
        mean_reward = np.mean(y)
        std_reward = np.std(y)

        print("Mean Reward: {:.2f}".format(mean_reward))
        print("STD Reward:", std_reward)

def test_clone_model(model=None, n_timesteps=10000, render=False):
    env = gym.make('RoboschoolReacher-v1')
    obs = env.reset()
    rewards = []
    running_reward = 0
    for _ in range(n_timesteps):
        obs_batch = np.expand_dims(obs, 0)
        action = model.predict_on_batch(obs_batch)

        obs, reward, done, _ = env.step(action[0])

        running_reward += reward

        if (render):
            env.render()

        if done:
            rewards.append(running_reward)
            # print(running_reward)
            running_reward = 0
            obs = env.reset()

    print("Mean Reward: {:.2f}".format(np.mean(rewards)))
    print("STD Reward:", np.std(rewards))

def create_clone_model():
    input_dims = 9
    output_dims = 2

    model = Sequential()
    model.add(Dense(output_dim=64, input_dim=input_dims, init='normal'))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=64, input_dim=64, init='normal'))
    model.add(Activation('relu'))
    model.add(Dense(output_dim=output_dims, input_dim=64, init='normal'))
    model.add(Activation('tanh'))
    model.compile(optimizer='adam', loss='mse', metrics=['accuracy'])
    model.summary()

    return model

def generate_datas(env, expert_policy, clone_policy, fraction_assist, num_datas):
    observations = []
    actions = []
    returns = []
    for i in range(num_datas):
        obs = env.reset()
        done = False
        totalr = 0.
        steps = 0
        while not done:

            # label expert data
            label = False
            if (fraction_assist > random.random()):
                action = expert_policy.predict(obs)
                label = True
                action = action[0]
            else:
                action = clone_policy.predict(obs)

            if (label == True):
                observations.append(obs[0])
                actions.append(action[0])

            obs, r, done, _ = env.step(action)
            totalr += r
            steps += 1

        returns.append(totalr)

    print('Return summary: mean=%f, std=%f' % (np.mean(returns), np.std(returns)))

    return observations, actions

def dagger():
    env = create_test_env('RoboschoolReacher-v1')
    expert_model = ALGOS['ppo2'].load('/home/tuyenple/rl_continuous/tested_environments/trained_agents/ppo2/RoboschoolReacher-v1.pkl', env=env)
    clone_model = create_clone_model()
    fraction_assist = 1.0

    labeled_obs = []
    labeled_actions = []

    for i in range(10):
        print('DAgger iter', i)
        if i != 0:
            fraction_assist -= 0.01

        obs, actions = generate_datas(env, expert_model, clone_model, fraction_assist, num_datas=100)
        labeled_obs += obs
        labeled_actions += actions

        if (len(labeled_obs) > 30000):
            labeled_obs = labeled_obs[len(labeled_obs)-30000:]
            labeled_actions = labeled_actions[len(labeled_actions)-30000:]

        clone_model.fit(np.array(labeled_obs), np.array(labeled_actions), epochs=100, batch_size=128, verbose=0)
        test_clone_model(clone_model, 10000, render=False)

    return clone_model

def main():
    trained_agents_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../trained_agents')
    logs_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../logs')
    parser = argparse.ArgumentParser()
    parser.add_argument('--env', help='environment ID', type=str, default='RoboschoolAnt-v1')
    parser.add_argument('-f', '--folder', help='Log folder', type=str, default=trained_agents_path)
    parser.add_argument('--algo', help='RL Algorithm', default='',
                        type=str, required=False, choices=list(ALGOS.keys()))
    parser.add_argument('-n', '--n-timesteps', help='number of timesteps', default=5000,
                        type=int)
    parser.add_argument('--n-envs', help='number of environments', default=1,
                        type=int)
    parser.add_argument('--exp-id', help='Experiment ID (default: -1, no exp folder, 0: latest)', default=-1,
                        type=int)
    parser.add_argument('--verbose', help='Verbose mode (0: no output, 1: INFO)', default=1,
                        type=int)
    parser.add_argument('--no-render', action='store_true', default=False,
                        help='Do not render the environment (useful for tests)')
    parser.add_argument('--deterministic', action='store_true', default=False,
                        help='Use deterministic actions')
    parser.add_argument('--norm-reward', action='store_true', default=False,
                        help='Normalize reward if applicable (trained with VecNormalize)')
    parser.add_argument('--seed', help='Random generator seed', type=int, default=0)
    parser.add_argument('--reward-log', help='Where to log reward', default=logs_path, type=str)
    parser.add_argument('--collect-data', action='store_true', default=False)

    args = parser.parse_args()

    env_id = args.env
    algo = args.algo
    folder = args.folder

    if args.exp_id == 0:
        args.exp_id = get_latest_run_id(os.path.join(folder, algo), env_id)
        print('Loading latest experiment, id={}'.format(args.exp_id))

    # Sanity checks
    if args.exp_id > 0:
        log_path = os.path.join(folder, algo, '{}_{}'.format(env_id, args.exp_id))
    else:
        log_path = os.path.join(folder, algo)

    model_path = "{}/{}.pkl".format(log_path, env_id)

    print(args.exp_id)
    # assert os.path.isdir(log_path), "The {} folder was not found".format(log_path)
    # assert os.path.isfile(model_path), "No model found for {} on {}, path: {}".format(algo, env_id, model_path)

    if algo in ['dqn', 'ddpg', 'sac']:
        args.n_envs = 1

    set_global_seeds(args.seed)

    is_atari = 'NoFrameskip' in env_id

    stats_path = os.path.join(log_path, env_id)
    hyperparams, stats_path = get_saved_hyperparams(stats_path, norm_reward=args.norm_reward, test_mode=True)

    if (algo == ''):
        algo_name = 'random'
    else:
        algo_name = algo
    reward_log_dir = os.path.join(args.reward_log, algo_name, env_id) if args.reward_log != '' else None

    env = create_test_env(env_id, n_envs=args.n_envs, is_atari=is_atari,
                          stats_path=stats_path, seed=args.seed, log_dir=reward_log_dir,
                          should_render=not args.no_render,
                          hyperparams=hyperparams)

    # ACER raises errors because the environment passed must have
    # the same number of environments as the model was trained on.
    load_env = None if algo == 'acer' else env

    if algo != '':
        model = ALGOS[algo].load(model_path, env=load_env)
    else:
        model = None

    obs = env.reset()

    # Force deterministic for DQN and DDPG
    deterministic = args.deterministic or algo in ['dqn', 'ddpg', 'sac']

    running_reward = 0.0
    ep_len = 0

    datas = {
        'expert_obs': [],
        'expert_action': [],
        'expert_reward': [],
        'expert_next_obs': []
    }
    for _ in range(args.n_timesteps):
        if model == None:
            # Random Agent
            action = [env.action_space.sample()]
        else:
            action, _ = model.predict(obs, deterministic=deterministic)
        if (args.collect_data):
            datas['expert_obs'].append(obs[0])
            datas['expert_action'].append(action[0])
        # Clip Action to avoid out of bound errors
        if isinstance(env.action_space, gym.spaces.Box):
            action = np.clip(action, env.action_space.low, env.action_space.high)

        obs, reward, done, infos = env.step(action)

        if (args.collect_data):
            datas['expert_reward'].append(reward)
            datas['expert_next_obs'].append(obs[0])

        if not args.no_render:
            env.render('human')
        running_reward += reward[0]
        ep_len += 1

        if args.n_envs == 1:
            # For atari the return reward is not the atari score
            # so we have to get it from the infos dict
            if is_atari and infos is not None and args.verbose >= 1:
                episode_infos = infos[0].get('episode')
                if episode_infos is not None:
                    print("Atari Episode Score: {:.2f}".format(episode_infos['r']))
                    print("Atari Episode Length", episode_infos['l'])

            if done and not is_atari and args.verbose >= 1:
                # NOTE: for env using VecNormalize, the mean reward
                # is a normalized reward when `--norm_reward` flag is passed
                print("Episode Reward: {:.2f}".format(running_reward))
                print("Episode Length", ep_len)
                running_reward = 0.0
                ep_len = 0

    # Workaround for https://github.com/openai/gym/issues/893
    if not args.no_render:
        if args.n_envs == 1 and 'Bullet' not in env_id and not is_atari:
            # DummyVecEnv
            # Unwrap env
            while isinstance(env, VecNormalize) or isinstance(env, VecFrameStack):
                env = env.venv
            env.envs[0].env.close()
        else:
            # SubprocVecEnv
            env.close()

    # Store data
    if (args.collect_data):
        # datas_df = pd.DataFrame(datas)
        # datas_df.to_csv('{}/data.csv'.format(reward_log_dir), sep=",", index=False)
        file = open('{}/data.csv'.format(reward_log_dir), 'wb')
        pickle.dump(datas, file)
        file.close()
        print("Saved results to {}/data.csv".format(reward_log_dir))

if __name__ == '__main__':
    main()
    # model = dagger()