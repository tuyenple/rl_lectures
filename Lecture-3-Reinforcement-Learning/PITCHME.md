---?image=assets/img/conference.png&position=bottom&size=100% 100%
@snap[midpoint span-65]
@box[bg-blue text-white rounded](Lecture 1#Imitation Learning)
@snapend

---
@snap[north-west]
**Goals**
@snapend

@snap[midpoint list-content-concise span-100]
@ul[list-bullets-circles]
- Understand definitions & notation
- Understand basic imitation learning algorithms
- Understand their strengths & weaknesses
@ulend
@snapend

---?image=assets/img/terminology.png&position=left&size=60% auto
@snap[north-west]
**Terminology & notation (1)**
@snapend
@snap[east span-35]
@ul[list-bullets-circles]
- State $s_t$
- Action $a_t$
- Observation $o_t$
- Policy $\pi(a_t|s_t)$
- Markov decision process (MDP),...
- Transition function $p(s_{t+1}|s_t,a_t)$
@ulend
@snapend

---?image=assets/img/terminology_1.png&position=center&size=auto 80%
@snap[north-west]
**Terminology & notation (2)**
@snapend
@snap[midpoint span-40]
@ul[list-bullets-circles]
- Sometimes, we can see the notations: $x_t$ and $u_t$ (in Robotics)
@ulend
@snapend

---?image=assets/img/cloning.png&position=center&size=auto 80%
@snap[north-west]
**Imitation: Behavioral Cloning (1)**
@snapend
@snap[midpoint span-100]
@ul[list-bullets-circles]
- Colect data from human (expert system) => training the policy using collected data (supervised learning)
@ulend
@snapend

---
@snap[north-west]
**Imitation: Behavioral Cloning (2)**
@snapend
@snap[midpoint span-100]
![Video](https://www.youtube.com/embed/qhUvQiKec2U)
@snapend
@snap[south span-100]
Example from NVIDIA autonomous car
@snapend

---
**Practice 1: Behavioral Cloning**

+++?image=assets/img/ppo2-RoboschoolReacher-v1-step-0-to-step-1000.gif&position=left&size=50% auto
@snap[north-west]
**Goals**
@snapend

@snap[east list-content-concise span-50]
@ul[list-bullets-circles]
- Build a model to control an **Reacher** using behavioral cloning method.
- Reacher environment: 9-dimensional states and 2-dimensional actions
@ulend
@snapend

+++
@snap[north-west]
**Step 1: Getting Set Up**
@snapend

Clone source code from gitlabs

```bash
git clone https://gitlab.com/agilesoda/rl_continuous.git
```

+++
@snap[north-west]
**Step 1: Getting Set Up**
@snapend

**cd** to the *rl_continuous* folder and install dependent packages from requirements file

```bash
sudo pip3 install requirement.txt
```

+++
@snap[north-west]
**Step 1: Getting Set Up**
@snapend

**cd** to the *tested_environments/imitation_learning* folder.

```bash
cd tested_environments/imitation_learning
```

+++

Run a random agent

```bash
python3 imitation.py --env RoboschoolReacher-v1 --n-timesteps 10000
```
@[1](*--env*: name of environment and *--n-timesteps*: number of timesteps)

+++
@snap[north-west]
**Step 1: Getting Set Up**
@snapend

The reward of the random agent is stored in monitor.csv file at *tested_environments/logs/random/RoboschoolReacher-v1*

Now, we show the performance of the random agent:

![alt](assets/img/imitation_random_agent.png)

+++
@snap[north-west]
**Step 1: Getting Set Up**
@snapend

Run an Reacher that is trained by PPO

```bash
python3 imitation.py --env RoboschoolReacher-v1 --algo ppo2 --n-timesteps 100000
```
@[1](*--algo*: using the Reacher trained by PPO algorithm)

+++
@snap[north-west]
**Step 1: Getting Set Up**
@snapend

Similarly, we show the reward of the trained agent

![alt](assets/img/imitation_ppo.png)

+++
@snap[north-west]
**Step 2: Collect data from expert.**
@snapend

Collect data from expert and store to file data.csv

```bash
python3 imitation.py --env RoboschoolReacher-v1 --algo ppo2 --n-timesteps 100000 --no-render --collect-data
```
@[1](*--no-render:* don't show animation and *--collect-data:* store the data to file)

+++
@snap[north-west]
**Step 2: Collect data from expert.**
@snapend

Show the data.

![alt](assets/img/imitation_data.png)

+++
@snap[north-west]
**Step 3: Build a clone behaviour model**
@snapend

@snap[west list-content-concise span-100]
@ul[list-bullets-circles](false)
- The model has:
    - input is **observations** (states)
    - output is **actions**
@ulend
@snapend

+++
@snap[north-west]
**Step 3: Build a clone behaviour model**
@snapend

The clone behaviour model

```python
input_dims = 9
output_dims = 2

model = Sequential()
model.add(Dense(output_dim=64, input_dim=input_dims, init='normal'))
model.add(Activation('relu'))
model.add(Dense(output_dim=64, input_dim=64, init='normal'))
model.add(Activation('relu'))
model.add(Dense(output_dim=output_dims, input_dim=64, init='normal'))
model.add(Activation('tanh'))
model.compile(optimizer='adam', loss='mse', metrics=['accuracy'])
model.summary()
```
@[5](model has 9-dimensional input states)
@[6-8](model has 2 hidden layers of 64 units)
@[11](use Adam optimizer and mean square error loss function)

+++
@snap[north-west]
**Step 3: Build a clone behaviour model**
@snapend

Model summary

![alt](assets/img/imitation_model_summary.png)

+++
@snap[north-west]
**Step 3: Build a clone behaviour model**
@snapend

Training the model using 10000 data samples (supervised learning)

```python
obs = np.array(datas['expert_obs'])
action = np.array(datas['expert_action'])
indices = np.random.permutation(100000)
training_idx, test_idx = indices[:9000], indices[9000:10000]

train_x, test_x = obs[training_idx,:], obs[test_idx,:]
train_y, test_y = action[training_idx,:], action[test_idx,:]

# Train the model
model.fit(train_x, train_y, epochs=100, batch_size=128, verbose=1)
```
@[1-7](Devide data to training set and testing set. We only use **observation** and **action** to train the model)
@[10](Train the model)

+++
@snap[north-west]
**Step 3: Build a clone behaviour model**
@snapend

Model accuracy

![alt](assets/img/imitation_accuracy_10000.png)

+++
@snap[north-west]
**Step 3: Build a clone behaviour model**
@snapend

Test performance of trained model

```python
def test_clone_model(model, n_timesteps):
    env = gym.make('RoboschoolReacher-v1')
    obs = env.reset()
    rewards = []
    running_reward = 0
    for _ in range(n_timesteps):
        obs_batch = np.expand_dims(obs, 0)
        action = model.predict_on_batch(obs_batch)
        
        obs, reward, done, _ = env.step(action[0])

        running_reward += reward

        if done:
            rewards.append(running_reward)
            running_reward = 0
            obs = env.reset()

    print("Mean Reward: {:.2f}".format(np.mean(rewards)))
    print("STD Reward:", np.std(rewards))
```
@[2](create gym environment)
@[6-17](loop *n_timesteps* to get the averaged reward and std)
@[8](use the trained model to predict the action)
@[13-17](when an episode is terminated, we store the episode reward and start a new episode)
@[19-20](Finally, we show the averaged reward and averaged std)

+++
@snap[north-west]
**Step 3: Build a clone behaviour model**
@snapend

Performance of the trained model.

![alt](assets/img/imitation_performance_trained_10000.png)

+++
@snap[north-west]
**Step 3: Build a clone behaviour model**
@snapend

Now we try to train the model again with more data (using 100000 samples). And see what happened ...

+++
@snap[north-west]
**Step 3: Build a clone behaviour model**
@snapend

The clone behaviour model

```python
input_dims = 9
output_dims = 2

model = Sequential()
model.add(Dense(output_dim=64, input_dim=input_dims, init='normal'))
model.add(Activation('relu'))
model.add(Dense(output_dim=64, input_dim=64, init='normal'))
model.add(Activation('relu'))
model.add(Dense(output_dim=output_dims, input_dim=64, init='normal'))
model.add(Activation('tanh'))
model.compile(optimizer='adam', loss='mse', metrics=['accuracy'])
model.summary()
```
@[1-12](We use the same model)

+++
@snap[north-west]
**Step 3: Build a clone behaviour model**
@snapend

The clone behaviour model

```python
obs = np.array(datas['expert_obs'])
action = np.array(datas['expert_action'])
indices = np.random.permutation(100000)
training_idx, test_idx = indices[:90000], indices[90000:]

train_x, test_x = obs[training_idx,:], obs[test_idx,:]
train_y, test_y = action[training_idx,:], action[test_idx,:]

# Train the model
model.fit(train_x, train_y, epochs=100, batch_size=128, verbose=1)
```
@[1-10](... but use 100.000 sample to train the model)

+++
@snap[north-west]
**Step 3: Build a clone behaviour model**
@snapend

Model accuracy

![alt](assets/img/imitation_train_100000.png)

+++
@snap[north-west]
**Step 3: Build a clone behaviour model**
@snapend

Performance of the trained model with 100.000 samples

![alt](assets/img/imitation_performance_100000.png)

---?image=assets/img/improve_cloning.png&position=left&size=40% auto
@snap[north-west]
**Limitation of Behavioral Cloning**
@snapend

@snap[east list-content-concise span-60]
@ul[list-bullets-circles]
- Depend on the expert systems (human) and cannot exceed the level of the expert systems
- Distributional "drift" problem (E.g. Distribution mismatch problem)
@ulend
@snapend

---?image=assets/img/dagger.png&position=left&size=40% auto
@snap[north-west]
**DAgger (Dataset Aggregation)**
@snapend

@snap[east list-content-concise span-60]
@ul[list-bullets-circles]
- DAgger algorithm reduces the mismatch between training data and experimence data
- Approach:
    - Human and clone model work together
    - Ask human to label the data executed by clone model
@ulend
@snapend

---
**Practice 2: Improve Behavioral Cloning using DAgger method**

+++
@snap[north-west]
**Goals**
@snapend

@snap[midpoint list-content-concise span-100]
@ul[list-bullets-circles](false)
- Implement Dagger method to improve the behavioral cloning model.
@ulend
@snapend

+++
@snap[north-west]
**Implemetation**
@snapend

The main source code of Dagger

```python
def dagger():
    env = create_test_env('RoboschoolReacher-v1')
    expert_model = ALGOS['ppo2'].load('/path/to/expert/model.pkl', env=env)
    clone_model = create_clone_model()
    fraction_assist = 1.0

    labeled_obs = []
    labeled_actions = []

    for i in range(10):
        print('DAgger iter', i)
        if i != 0:
            fraction_assist -= 0.01

        obs, actions = generate_datas(env, expert_model, clone_model, fraction_assist, num_datas=100)
        labeled_obs += obs
        labeled_actions += actions

        if (len(labeled_obs) > 30000):
            labeled_obs = labeled_obs[len(labeled_obs)-30000:]
            labeled_actions = labeled_actions[len(labeled_actions)-30000:]

        clone_model.fit(np.array(labeled_obs), np.array(labeled_actions), epochs=100, batch_size=128, verbose=0)
        test_clone_model(clone_model, 10000, render=False)

    return clone_model
```
@[2](create the environement)
@[3](load the expert model. In practice, this is a human)
@[4](create the clone model)
@[5](*fraction_assist*: a variable to control the level of human interaction)
@[7-8](variable to store training data)
@[10-24](For each iteration, collecting the data, labeled data, traing the model)
@[12-13](For each iteration, reducing the level of human interaction)
@[15-17](Collecting the data. <br>Use both expert model and clone model to take actions)
@[19-21](Only use the last 30.000 samples to train the model)
@[23](Training the model)
@[24](We test performance of clone model every iteration)
@[26](Return the model)

+++
@snap[north-west]
**Final performance**
@snapend

Performance of the clone model after 10 iterations

![alt](assets/img/imitation_performance_dagger_10.png)

+++
**Summary**

![alt](assets/img/imitation_method_comparision.png)

---
@snap[north-west]
**Conclusion: Imitation Learning**
@snapend

@snap[midpoint list-content-concise span-100]
@ul[list-bullets-circles]
- Work best for the problems which have a stable distribution
- Disadvantages: distribution mismatch problem
- How to improve:
    - Dagger approach: ask human to label the data during training
@ulend
@snapend

@snap[south list-content-concise span-100]
![alt](assets/img/recap.png)
@snapend

---
@snap[north-west]
**Do it by your self**
@snapend

Practice imitation learning algorithms by yourself (Select any trained environments from the gitlab repositories)

+++
Inverted Double Pendulum

![alt](assets/img/ppo2-RoboschoolInvertedDoublePendulum-v1-step-0-to-step-1000.gif)

+++
Bipedal Walker

![alt](assets/img/BipedalWalker-v2.gif)

+++
Bipedal Walker Hardcore

![alt](assets/img/ppo2-BipedalWalkerHardcore-v2-step-0-to-step-1000.gif)

+++
Lunar Lander Continuous

![alt](assets/img/ppo2-LunarLanderContinuous-v2-step-0-to-step-1000.gif)

+++
Minitaur

![alt](assets/img/ppo2-MinitaurBulletEnv-v0-step-0-to-step-1000.gif)

+++
Ant

![alt](assets/img/ppo2-RoboschoolAnt-v1-step-0-to-step-1000.gif)

+++
HalfCheetah

![alt](assets/img/ppo2-RoboschoolHalfCheetah-v1-step-0-to-step-1000.gif)

+++
Hopper

![alt](assets/img/ppo2-RoboschoolHopper-v1-step-0-to-step-1000.gif)

+++
Humanoid Flag run

![alt](assets/img/ppo2-RoboschoolHumanoidFlagrun-v1-step-0-to-step-1000.gif)

+++
Humanoid

![alt](assets/img/ppo2-RoboschoolHumanoid-v1-step-0-to-step-1000.gif)


+++
Inverted Pendulum Swingup

![alt](assets/img/ppo2-RoboschoolInvertedPendulumSwingup-v1-step-0-to-step-1000.gif)

+++
Walker2d

![alt](assets/img/ppo2-RoboschoolWalker2d-v1-step-0-to-step-1000.gif)

---
@snap[north-west]
**What next ...**
@snapend
- Reinforcement learning
    - By integrating reward function
- Policy gradient algorithms

---
@snap[north-west]
**Case studies (1)**
@snapend
@snap[midpoint span-100]
![Video](https://www.youtube.com/embed/umRdt3zGgpU)
@snapend
@snap[south span-100]
**A Machine Learning Approach to Visual Perception of Forest Trails for Mobile Robots** *IEEE Robotics and Automation Letters (RA-L)*, pages 661 - 667, 2016
@snapend

@snap[north-west]
**Case studies**
@snapend
@snap[midpoint span-100]
![Video](https://www.youtube.com/embed/umRdt3zGgpU)
@snapend
@snap[south span-100]
**A Machine Learning Approach to Visual Perception of Forest Trails for Mobile Robots** *IEEE Robotics and Automation Letters (RA-L)*, pages 661 - 667, 2016
@snapend

---
**Thank you**