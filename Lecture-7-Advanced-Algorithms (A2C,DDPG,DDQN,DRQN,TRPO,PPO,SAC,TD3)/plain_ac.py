import os
import time
import pandas as pd
import re
import shutil
import math
from sklearn.metrics import confusion_matrix , roc_curve , precision_recall_curve
import matplotlib
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
from tqdm import tqdm

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

from tf.common import *
os.environ["CUDA_VISIBLE_DEVICES"]="0"

plt.ion()
fig = plt.figure(figsize=(30., 15.))
ax = fig.add_subplot(311, facecolor="#EAEAEA")
ax2 = fig.add_subplot(312, facecolor="#EAEAEA")
ax3 = fig.add_subplot(313, facecolor="#EAEAEA")

def plotting(data, data_loss, path, file_name="model"):
    ax.cla()
    ax2.cla()
    ax3.cla()

    ax.set_title("Performance", fontsize=20, fontweight="bold")
    ax2.set_title("entropy on train", fontsize=20, fontweight="bold")
    ax3.set_title("reward on train", fontsize=20, fontweight="bold")
    ax.tick_params(axis="both", which="major", labelsize=20)
    ax2.tick_params(axis="both", which="major", labelsize=20)
    ax3.tick_params(axis="both", which="major", labelsize=20)
    ax.grid(True)
    ax2.grid(True)
    ax3.grid(True)

    x_axis = data.loc[:, "Iter"]
    loss_x_axis = data_loss.loc[:, "Iter"]

    ax.plot(x_axis, data.loc[:, "tr_for_TT"], color="#A94CAF", label="tr_for_TT", marker="o", markersize=4,
            linestyle=":")
    ax.plot(x_axis, data.loc[:, "va_for_TT"], color="#A94CAF", label="va_for_TT", marker="o", markersize=4)
    ax.plot(x_axis, data.loc[:, "tr_for_TF"], color="#118DF0", label="tr_for_TF", marker="o", markersize=4,
            linestyle=":")
    ax.plot(x_axis, data.loc[:, "va_for_TF"], color="#118DF0", label="va_for_TF", marker="o", markersize=4)
    ax.plot(x_axis, data.loc[:, "tr_for_FT"], color="#FF4B68", label="tr_for_FT", marker="o", markersize=4,
            linestyle=":")
    ax.plot(x_axis, data.loc[:, "va_for_FT"], color="#FF4B68", label="va_for_FT", marker="o", markersize=4)
    ax.plot(x_axis, data.loc[:, "tr_for_FF"], color="#13c4a3", label="tr_for_FF", marker="o", markersize=4,
            linestyle=":")
    ax.plot(x_axis, data.loc[:, "va_for_FF"], color="#13c4a3", label="va_for_FF", marker="o", markersize=4)
    ax2.plot(loss_x_axis, data_loss.loc[:, "entropy"], color="brown", label="entropy", marker="o", markersize=1)
    ax3.plot(loss_x_axis, data_loss.loc[:, "reward"], color="green", label="reward", marker="o", markersize=1)

    ax.legend(bbox_to_anchor=(1.01, 1), loc=2, fontsize=15, borderaxespad=0.1, edgecolor="black", facecolor="#EAEAEA")
    ax2.legend(bbox_to_anchor=(1.01, 1), loc=2, fontsize=15, borderaxespad=0.1, edgecolor="black", facecolor="#EAEAEA")
    ax3.legend(bbox_to_anchor=(1.01, 1), loc=2, fontsize=15, borderaxespad=0.1, edgecolor="black", facecolor="#EAEAEA")

    plt.pause(0.001)

    # 해당 경로에 출력되는 plot 과 각 iteration 별 모형 성능들이 기록된 log를 csv  형식으로 저장
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=True, mode=0o777)
    fig.savefig(path + "/" + file_name + "_last_update" + ".png")
    data.to_csv(path + "/" + file_name + "_last_update" + "_data_log.csv")
    data_loss.to_csv(path + "/" + file_name + "_last_update" + "_data_loss_log.csv")

def data_trans(data, set_var, cols=None, data_idx=["train", "valid"][0],
               data_info=None, na_cols=True, standardize=True):
    "factor형 변수를 지정"
    fac_var = [x for x in set_var["in_var"] if x not in set_var["num_var"]]

    if fac_var != []:
        "one-hot encoding 을 수행할 변수명을 지정"
        cols = fac_var if cols is None else [x for x in cols if x in fac_var]

    data_name_idx = data.columns

    if data_idx == "train":

        if fac_var != []:
            # factor 변수 중 결측치가 zz 로 처리된 경우 zz_cols 에  , 그 외에는 na_cols 에 변수명을 저장
            ZZ_cols = pd.DataFrame(cols).loc[[any(data[str(i)] == "ZZ") for i in cols]][0].tolist()
            na_cols = [x for x in cols if x not in ZZ_cols]

            # argument 에서 na_cols 의 설정에 따라 one-hot encoding 시 na level 생성 여부를 지정
            data = pd.get_dummies(data, columns=na_cols, dummy_na=na_cols)
            data = pd.get_dummies(data, columns=ZZ_cols, dummy_na=False)

            ## one-hot encoding 으로 생성된 변수명을 one-hot var 에 저장
            one_hot_var = [x for x in data.columns if x not in data_name_idx]

            ## 숫자변수 표준화를 수행
            if standardize is True:
                norm_mean = pd.np.nanmean(data.loc[:, set_var["num_var"]], axis=0)
                norm_std = pd.np.nanstd(data.loc[:, set_var["num_var"]], axis=0, ddof=1)

                data.loc[:, set_var["num_var"]] = (data.loc[:, set_var["num_var"]] - norm_mean) / norm_std

            else:
                norm_mean = norm_std = None

                ## 데이터 처리 정보를 저장
            data_info = {"x_var": (set_var["num_var"] + one_hot_var),
                         "y_var": set_var["target_var"],
                         "kgb_var": set_var["kgb_var"],
                         "fnl_info_var": set_var["fnl_info_var"],
                         "reward_amt_var": set_var["reward_amt_var"],

                         "fac_var": fac_var,
                         "num_var": num_var,
                         "ZZ_cols": ZZ_cols,
                         "na_cols": na_cols,
                         "one_hot_var": one_hot_var,

                         "standardize": standardize,
                         "norm_mean": norm_mean,
                         "norm_std": norm_std
                         }

        else:
            ## 숫자변수 표준화를 수행
            if standardize is True:
                norm_mean = pd.np.nanmean(data.loc[:, set_var["num_var"]], axis=0)
                norm_std = pd.np.nanstd(data.loc[:, set_var["num_var"]], axis=0, ddof=1)

                data.loc[:, set_var["num_var"]] = (data.loc[:, set_var["num_var"]] - norm_mean) / norm_std

            else:
                norm_mean = norm_std = None
                ## 데이터 처리 정보를 저장
            data_info = {"x_var": (set_var["num_var"]),
                         "y_var": set_var["target_var"],
                         "reward_amt_var": set_var["reward_amt_var"],
                         "num_var": set_var["num_var"],
                         "standardize": standardize,
                         "norm_mean": norm_mean,
                         "norm_std": norm_std
                         }
        return data, data_info

    elif data_idx == "valid":
        if fac_var != []:
            # train set 에서 생성된 data info 를 이용하여 데이터를 처리함
            inter_ZZ_cols = [x for x in data_info["ZZ_cols"] if x in data.columns]
            inter_na_cols = [x for x in data_info["na_cols"] if x in data.columns]

            # pandas 패키지의 get_dummies 를 이용하여 one_hot encoding 실시
            data = pd.get_dummies(data, columns=inter_na_cols, dummy_na=na_cols)
            data = pd.get_dummies(data, columns=inter_ZZ_cols, dummy_na=False)

            ## factor new level 처리
            one_hot_var = [x for x in data.columns if x not in data_name_idx]
            # train set 에서 없엇던 level 체크
            only_on_valid = [x for x in one_hot_var if x not in data_info["x_var"]]
            # train set 에는 있었지만 validation set 에는 없엇던 level 체크
            not_on_valid = [x for x in data_info["x_var"] if x not in data.columns]
            data = data.loc[:, [x for x in data.columns if x not in only_on_valid]]
            # train set 에는 없엇던 level 은 ZZ_cols 와 na_cols 를 확인하여 처리
            if len(only_on_valid) > 0:

                for i in only_on_valid:
                    temp_var = re.split("_[0-9a-zA-Z]*$", only_on_valid[0])[0]

                    if temp_var in data_info["ZZ_cols"]:
                        data.loc[:, temp_var + "_ZZ"] = data[i] + data[temp_var + "_ZZ"]
                    else:
                        data.loc[:, temp_var + "_nan"] = data[i] + data[temp_var + "_nan"]

                    # train set 에는 있엇지만 validation set 에는 없는 leve 은 새로 생성후 모든 값을 0 으로 지정
            if len(not_on_valid) > 0:
                for i in not_on_valid:
                    data[str(i)] = 0
        else:

            # 숫자변수 표준화를 수행 train_set 의 평균과 분산 정보를 이용
            if data_info["standardize"] is True:
                data.loc[:, set_var["num_var"]] = (data.loc[:, set_var["num_var"]] - data_info["norm_mean"]) / \
                                                  data_info["norm_std"]

        return data

def progress_bar(count, total):
    count += 1
    bar_len = 75
    filled_len = int(round(bar_len + count / float(total)))

    percents = round(100 * count / float(total), 1)

    bar = "=" * filled_len + "-" * (bar_len - filled_len)

    print("[%s] %s%s" % (bar, percents, " %"), end="\r")

def predict(sess, model, data, model_info, set_var, batch_size=5000, on_train=False):
    x_var = model_info["x_var"]
    y_var = model_info["y_var"]

    add_var_train = pd.DataFrame((sum([set_var[str(i)] for i in ["target_var"]], [])))[0].unique().tolist()

    add_var = pd.DataFrame((sum([set_var[str(i)] for i in ["target_var", "key_var", "scale_amt_var"]], [])))[
        0].unique().tolist()

    # train 중이 아닌 predcit 인 경우에 valid set 에 대해서 data setting 을 함

    # if on_train is False:
    #     data = data_trans(data, set_var, data_idx="valid", data_info=model_info)
    #     data.reset_index(inplace=True, drop=True)

    res_out_rl_pred = np.zeros(data.shape[0], dtype=np.int64)
    res_out_probs  = np.zeros([data.shape[0],2], dtype=np.float32)
    if on_train is False:
        res_out_rewards = np.zeros(data.shape[0], dtype=np.float32)

    # predict 진행시 각 스텝마다의 batch 에 대한 predict 횟수를 정의
    batch_size = [data.shape[0] if data.shape[0] < batch_size else batch_size][0]

    loop_n = data.shape[0] // batch_size
    loop_last = data.shape[0] - batch_size * loop_n
    loop_n = [loop_n if (loop_last == 0) else loop_n + 1][0]

    step = 0

    check_time = time.time()

    if (on_train is False):
        pbar = tqdm(total=loop_n)

    for loop_i in range(loop_n):
        if (loop_i == (loop_n - 1)) and (loop_last != 0):
            batch_size = loop_last

        step += batch_size

        if (on_train is False):
            pbar.update(1)

        # 저장된 모델에서 나오는 각 액션의 확률들 중 가장 큰 값에 해당하는 액션을 예측함
        states = data.loc[range(step - batch_size, step), x_var].values

        rl_pred_softmax, probs_t = sess.run([model.pi, model.probs], feed_dict={model.x_ph: states, model.tau_ph:1.0})

        # probs_t = sess.run(model.probs, feed_dict={model.x_ph: states, model.tau_ph: 1.0})
        # rl_pred_softmax = np.argmax(probs_t, axis=1)

        res_out_rl_pred[step - batch_size:step] = rl_pred_softmax
        res_out_probs[step - batch_size:step,:] = probs_t

        if on_train is False:
            rewards = [Reward(res_out_rl_pred[i], data.loc[i, y_var[0]], data.loc[i, reward_amt_var[0]]) for i in range(step - batch_size,step)]
            res_out_rewards[step - batch_size:step] = np.array(rewards)

    end_time = time.time()
    calc_time = end_time
    if (on_train is False):
        pbar.close()

    # 결과 저장
    rl_pred_out_train = pd.concat([pd.DataFrame(res_out_rl_pred), pd.DataFrame(res_out_probs),
                                   pd.DataFrame(data.loc[:, (add_var_train)])], axis=1)

    rl_pred_out_train.columns = (["rl_pred_softmax", "rl_p0", "rl_p1"] + add_var_train)

    for_scale = rl_pred_out_train

    # 모형 성능 계산
    ##############################################################################################

    for_TT = confusion_matrix(y_pred=for_scale['rl_pred_softmax'].astype(str), y_true=for_scale["Class"])[0, 0]
    for_TF = confusion_matrix(y_pred=for_scale['rl_pred_softmax'].astype(str), y_true=for_scale["Class"])[0, 1]
    for_FT = confusion_matrix(y_pred=for_scale['rl_pred_softmax'].astype(str), y_true=for_scale["Class"])[1, 0]
    for_FF = confusion_matrix(y_pred=for_scale['rl_pred_softmax'].astype(str), y_true=for_scale["Class"])[1, 1]

    ##############

    end_time = time.time()
    calc_time = end_time - calc_time
    total_time = end_time - check_time

    res_out_perf = {
        "for_TT": for_TT,
        "for_TF": for_TF,
        "for_FT": for_FT,
        "for_FF": for_FF
    }

    ## predeict 시에는 세부 타겟 별 검출력 및 policy에 의한 confusion matrix 가 출력이 되며 , csv 로 저장할 때 필요한 변수를 가져옴
    if on_train is False:
        rl_pred_out = pd.concat([pd.DataFrame(res_out_rl_pred),
                                 pd.DataFrame(res_out_probs),
                                 pd.DataFrame(res_out_rewards),
                                 pd.DataFrame(data.loc[:, (add_var)])], axis=1)

        rl_pred_out.columns = (["rl_pred_softmax", "rl_p0", "rl_p1", "rewards"] + add_var)
        confusion = confusion_matrix(y_pred=rl_pred_out["rl_pred_softmax"].astype(str),
                                     y_true=rl_pred_out[y_var[0]].astype(str))
        print(confusion)

        return rl_pred_out, res_out_perf
    return res_out_perf

########################################################################################################################
# reward function define

########################################################################################################################
def Reward(new_pred, real_target, scale_amt):
    # 실제 타겟이 bad인 경우
    if real_target == "1":
        if real_target != str(new_pred):
            get_reward = -scale_amt * 8 * 10
        else:
            get_reward = scale_amt * 3 * 10

    # 실제 타겟이 good인 경우
    elif real_target == "0":
        if real_target != str(new_pred):
            get_reward = -scale_amt * 0.0063 * 2 * 10
        else:
            get_reward = scale_amt * 0.0063 * 1 * 10

    return get_reward

def time_print(all_count, count, learn_time, batch_time, remain_time):
    return ("# learning " + ["    elapsed" if all_count - 1 != count else "done_in"][0] + " %-2.2f" +
            ["sec" if learn_time <= 60 else "min" if learn_time <= 3600 else "hour"][0] + "# %-2.2f " +
            ["sec" if batch_time <= 60 else "min" if batch_time <= 3600 else "hour"][0] + " per loop" +
            "# Estimated remain time :    %-2.2f" +
            ["sec" if remain_time <= 60 else "min" if remain_time <= 3600 else "hour"][0]) \
           % tuple([learn_time if learn_time <= 60 else round(learn_time / 60, 2) if learn_time <= 3600 else round(
        learn_time / 3600, 2)] + \
                   [batch_time if batch_time <= 60 else round(batch_time / 60, 2) if batch_time <= 3600 else round(
                       batch_time / 3600, 2)] + \
                   [remain_time if remain_time <= 60 else round(remain_time / 60, 2) if remain_time <= 3600 else round(
                       remain_time / 3600, 2)])

class Model():

    def __init__(self, ob_dim, ac_dim):
        # ========================================================================================#
        # Placeholders
        # ========================================================================================#
        # Observations
        self.x_ph = tf.placeholder(shape=[None, ob_dim], name="ob", dtype=tf.float32)
        # Actions
        self.a_ph = tf.placeholder(shape=[None], name="ac", dtype=tf.int32)
        # Advantages
        self.adv_ph = tf.placeholder(shape=[None], name="adv", dtype=tf.float32)
        self.ret_ph = tf.placeholder(shape=[None], name="ret", dtype=tf.float32)
        self.tau_ph = tf.placeholder(shape=None, name="tau", dtype=tf.float32)

        # ========================================================================================#
        # Policy
        # ========================================================================================#
        with tf.variable_scope('model'):
            with tf.variable_scope('pi'):
                # Compute stochastic policy over discrete actions
                self.logits = mlp_finance(self.x_ph, ac_dim, hidden_sizes, hidden_activation, output_activation=None,
                                     use_bias=use_bias, initializer=initializer)
                self.probs = tf.nn.softmax(self.logits / self.tau_ph)
                self.clipped_probs = tf.clip_by_value(self.probs, 1e-10, 1.0)
                self.logp_all = tf.log(self.clipped_probs)
                self.pi = tf.squeeze(tf.multinomial(self.logits / self.tau_ph, 1), axis=1)
                self.logp = tf.reduce_sum(tf.one_hot(self.a_ph, depth=ac_dim, dtype=tf.float32) * self.logp_all, axis=1)

                self.entropy = -tf.reduce_sum(self.clipped_probs * tf.log(self.clipped_probs), axis=-1)

            # Value-network
            with tf.variable_scope('v'):
                self.v = tf.squeeze(
                    mlp_finance(self.x_ph, 1, hidden_sizes, hidden_activation=hidden_activation, output_activation=None,
                                use_bias=use_bias, initializer=initializer), axis=1)

        # Loss function
        pi_loss = -self.logp * self.adv_ph - beta * self.entropy

        # OK: mean_squared_error, absolute_difference, huber_loss
        '''
        Tensorflow implementation of smooth L1 loss defined in Fast RCNN:
            (https://arxiv.org/pdf/1504.08083v2.pdf)

                        0.5 * (sigma * x)^2         if |x| < 1/sigma^2
        smoothL1(x) = {
                        |x| - 0.5/sigma^2           otherwise
        '''
        sigma = 1.0
        x = self.v - self.ret_ph
        conditional = tf.less(tf.abs(x), 1 / sigma ** 2)
        close = 0.5 * (sigma * x) ** 2
        far = tf.abs(x) - 0.5 / sigma ** 2
        v_loss = tf.where(conditional, close, far)

        self.params = find_trainable_variables("model")

        # Optimizer for value function
        self.train_v = tf.train.AdamOptimizer(learning_rate=vf_lr).minimize(v_loss)
        self.train_pi = tf.train.AdamOptimizer(learning_rate=pi_lr).minimize(pi_loss)

    def save_model(self, sess, fname="last_model", model_info=None):
        params = sess.run(self.params)
        _, ext = os.path.splitext(fname)
        if ext == "":
            fname += ".pkl"

        with open(fname, "wb") as file_:
            pickle.dump((model_info, params), file_)

    def load_model(self, sess, fname="last_model"):
        with open(fname, "rb") as file:
            model_info, params = pickle.load(file)

        restores = []
        for param, loaded_p in zip(self.params, params):
            restores.append(param.assign(loaded_p))
        sess.run(restores)

        return model_info

if __name__ == '__main__':
    # Hyperparameters
    seed = 123
    exp_name='ac'
    gamma=0.99
    steps_per_epoch=1000
    epochs=50000
    hidden_sizes=[50, 25, 10]
    hidden_activation=tf.nn.selu
    pi_lr=1e-3
    vf_lr=1e-3
    env_name="transaction"
    use_bias=False
    initializer=tf.initializers.variance_scaling(seed=seed)

    bad_rate = 0.1
    stop_exploration_rate = 0.3
    beta = 0.2
    tau = 20
    visual=True
    log_save_cycle=10000
    print_cycle=10000

    is_train = False

    log_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'logs', exp_name, env_name, "gamma="+str(gamma), "badrate="+str(bad_rate), "seed="+str(seed))
    # Set random seeds
    tf.reset_default_graph()
    tf.set_random_seed(seed)
    np.random.seed(seed)

    ########################################################################################################################
    # data load
    ########################################################################################################################
    ori_dat = pd.read_csv("./creditcard.csv", dtype="object")
    print(ori_dat.head())

    ########################################################################################################################
    # data munging & variable selection
    # Setting a Variable
    # in_var : input variable
    # num_var : numeric variable
    # fac_var ?: factor type variable during input (automatically specified by the data_trans function of rl_function, not specified here)
    # key_var : key variable
    # sep_idx_var : Key variable to divide training set and validation set
    # target_var : target variable
    # kgb_var ?: Inference data if the known good-bad (kgb) is equal to or less than 1 kgb 0
    # fnl_info_var ?: Details of the target
    # return_amt_var ?: variable that has max standardized the amt value
    # amt_var : amount variable
    # Nice_var ?: Score variable
    # Pred_target ?: Detailed target information
    ########################################################################################################################
    ori_dat['sep_idx'] = np.random.rand(len(ori_dat)) < 0.7
    ori_dat.sep_idx = ori_dat.sep_idx.astype(int)

    set_var = {
        "in_var": ['Time', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V7', 'V8', 'V9', 'V10',
                   'V11', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17', 'V18', 'V19', 'V20',
                   'V21', 'V22', 'V23', 'V24', 'V25', 'V26', 'V27', 'V28', 'Amount'],
        "num_var": ['Time', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V7', 'V8', 'V9', 'V10',
                    'V11', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17', 'V18', 'V19', 'V20',
                    'V21', 'V22', 'V23', 'V24', 'V25', 'V26', 'V27', 'V28', 'Amount'],
        "key_var": ['V1'],
        "sep_idx_var": ['sep_idx'],
        "target_var": ['Class'],
        "reward_amt_var": ['reward_amt'],
        "scale_amt_var": ['reward_amt']
    }

    ########################################################################################################################
    # 데이터의 숫자형 factor 형 각각 지정함
    # 숫자형 변수중 na 가 있는 변수에 대해서는 평균 값 대체를 함 [GRAS_SCORE]
    ########################################################################################################################
    ori_dat[(set_var["num_var"])] = ori_dat[(set_var["num_var"])].astype("float64")

    ########################################################################################################################
    # 개발 검증 셋 나눔
    # Note : Exam1
    ########################################################################################################################
    train_data = ori_dat.loc[ori_dat[set_var["sep_idx_var"][0]] == 1,
                             pd.DataFrame((sum([set_var[str(i)] for i in ["in_var", "target_var"]], [])))[
                                 0].unique().tolist()]

    valid_data = ori_dat.loc[ori_dat[set_var["sep_idx_var"][0]] == 0,
                             pd.DataFrame((sum([set_var[str(i)] for i in ["in_var", "target_var"]], [])))[
                                 0].unique().tolist()]
    train_data.reset_index(inplace=True, drop=True)
    valid_data.reset_index(inplace=True, drop=True)
    ########################################################################################################################
    # Standardize the parameters of loan amount application for each set
    ########################################################################################################################
    train_data = train_data.assign(
        reward_amt=(train_data["Amount"].astype("float64") / max(train_data["Amount"].astype("float64"))))
    valid_data = valid_data.assign(
        reward_amt=(valid_data["Amount"].astype("float64") / max(valid_data["Amount"].astype("float64"))))
    print(train_data.shape)
    print(valid_data.shape)

    train_data, data_info = data_trans(train_data, set_var)
    train_data.reset_index(inplace=True, drop=True)
    valid_data = data_trans(valid_data, set_var, data_idx="valid", data_info=data_info)
    valid_data.reset_index(inplace=True, drop=True)

    x_var = data_info["x_var"]
    y_var = data_info["y_var"]
    reward_amt_var = data_info["reward_amt_var"]
    norm_mean = data_info["norm_mean"]
    norm_std = data_info["norm_std"]

    #========================================================================================#
    # Space
    #========================================================================================#
    ob_dim = len(data_info["x_var"])
    ac_dim = 2

    model = Model(ob_dim, ac_dim)

    # Start tensorflow
    sess = tf.Session()
    sess.__enter__()  # equivalent to `with sess:`
    sess.run(tf.global_variables_initializer())

    start_time = time.time()

    if (is_train):
        count = -1
        valid_log = []
        loss_log = []
        batch_time_log = []

        # good 과 bad 의 비율을 조절하기 위해 good과 bad의 인덱스를 저장함
        good_idx = train_data.loc[(train_data[y_var[0]] == "0"),].index.values
        bad_idx = train_data.loc[(train_data[y_var[0]] == "1"),].index.values

        all_count = epochs

        tau_decay = (1 / tau) ** (1 / all_count * stop_exploration_rate)

        learn_time = 0.0
        model_info = {"x_var": x_var, "y_var": y_var, "standardize": True, "norm_mean": norm_mean, "norm_std":norm_std}
        batch_time = 0.0
        remain_time = 0.0

        pbar = tqdm(total=all_count)
        for epoch in range(epochs):
            count += 1

            iter_good = np.random.choice(good_idx, (steps_per_epoch - math.ceil(steps_per_epoch * bad_rate)))
            iter_bad = np.random.choice(bad_idx, math.ceil(steps_per_epoch * bad_rate))
            iter_idx = np.concatenate((iter_good, iter_bad))
            np.random.shuffle(iter_idx)

            tau = max([tau * tau_decay, 1])

            states = train_data.loc[:, x_var].iloc[iter_idx].values

            # way 1 (same performance)
            actions, state_values = sess.run([model.pi, model.v], feed_dict={model.x_ph: states, model.tau_ph: tau})
            # way 2 (same performance)
            # probs_t = sess.run(model.probs, feed_dict={model.x_ph:states, model.tau_ph:tau})
            # actions = np.array([np.random.choice(ac_dim, p=probs_i) for probs_i in probs_t])
            real_target = train_data.loc[iter_idx, y_var[0]].values
            scale_amt = train_data.loc[iter_idx, reward_amt_var[0]].values
            rewards = [Reward(actions[i], real_target[i], scale_amt[i]) for i in range(len(actions))]

            discounted_r = np.zeros_like(rewards, dtype=np.float32)
            total_r = 0
            rollings = 0
            for t in reversed(range(len(rewards))):
                if rewards[t] < 0:
                    rollings = 0
                rollings = rollings * gamma + rewards[t]
                discounted_r[t] = rollings
                total_r += rewards[t]

            discounted_r = (discounted_r - np.mean(discounted_r)) / (np.std(discounted_r)+eps)

            # state_values = sess.run(model.v, feed_dict={model.x_ph:states})

            advantages = discounted_r - state_values
            advantages = (advantages - np.mean(advantages)) / (np.std(advantages)+eps)
            inputs_ph = {model.x_ph: states,
                         model.a_ph: actions,
                         model.tau_ph: tau,
                         model.adv_ph: advantages,
                         model.ret_ph: discounted_r}

            # Value function learning
            sess.run(model.train_v, feed_dict=inputs_ph)

            # Policy gradient step
            _, ent = sess.run([model.train_pi, model.entropy], feed_dict=inputs_ph)

            loss_log.append([count, total_r/len(rewards), np.mean(ent)])
            loss_log_df = pd.DataFrame(loss_log, columns=["Iter", "reward", "entropy"])

            pbar.update(1)
            ###########################################################################################################################
            ''' 모델들의 정보를 출력함 training 중이기 때문에, on_train = True로 default 지정'''
            if ((count % print_cycle == 0)) or (count == all_count - 1):
                train_rl_perf = predict(sess, model, data=train_data, model_info=model_info, set_var=set_var,
                                        on_train=True)

                '''검증셋 predict'''
                rl_perf = predict(sess, model, data=valid_data, model_info=model_info, set_var=set_var,
                                  on_train=True)

                tr_for_TT = train_rl_perf['for_TT']
                va_for_TT = rl_perf['for_TT']
                tr_for_TF = train_rl_perf['for_TF']
                va_for_TF = rl_perf['for_TF']
                tr_for_FT = train_rl_perf['for_FT']
                va_for_FT = rl_perf['for_FT']
                tr_for_FF = train_rl_perf['for_FF']
                va_for_FF = rl_perf['for_FF']

                valid_log.append(
                    [count, train_rl_perf['for_TT'], rl_perf['for_TT'], train_rl_perf['for_TF'], rl_perf['for_TF'],
                     train_rl_perf['for_FT'], rl_perf['for_FT'], train_rl_perf['for_FF'], rl_perf['for_FF']])

                valid_log_df = pd.DataFrame(valid_log,
                                            columns=["Iter", "tr_for_TT", "va_for_TT", "tr_for_TF", "va_for_TF",
                                                     "tr_for_FT", "va_for_FT", "tr_for_FF", "va_for_FF"])
            #########################################################################################################################

            '''학습시간 계산에 필요한 값들 구함'''
            now_time = time.time()
            learn_time = now_time - start_time
            batch_time = learn_time / (count + 1)
            batch_time_log.append(batch_time)
            batch_time_mean = np.nanmean(batch_time_log)
            remain_time = batch_time_mean * (all_count - count - 1)

            '''training 중 출력하는 cycle과 출력할 대상들을 지정함'''
            if (count % print_cycle == 0) or (count == all_count - 1):
                if ((visual is True) and valid_log_df.shape[0] > 1) or (count == all_count - 1):
                    plotting(data=valid_log_df, data_loss=loss_log_df, path=log_path)

                '''세부 타켓별 검출력이 max인 값을 지정'''
                '''위에서 지정한 성능 및 시간들을 실제 print 해주는 부분'''
                print(time_print(all_count, count, learn_time, batch_time, remain_time),
                      ''' \n
                 New Loop : %-8d               [[ NOTE : best iter : mean of train and valid]]
                 ----------------------------------------------------------------------------------------- \n
                 perf >
                      ''' % (count), end="\r")

            '''사용자가 지정한 log_save_cycle 마다 모형을 저장 함'''
            if (count % log_save_cycle == 0 and count > 0):
                model.save_model(sess, os.path.join(log_path,'model_' + str(count)), model_info)

        '''Save last model'''
        plotting(data=valid_log_df, data_loss=loss_log_df, path=log_path)
        model.save_model(sess, os.path.join(log_path, 'last_model'), model_info)
        with open(log_path + "/time_info.txt", "w") as f:
            f.write(time_print(all_count, count, learn_time, batch_time, remain_time))

        pbar.close()
    else:
        ########################################################################################################################
        # Validation
        ########################################################################################################################
        # Load model
        model_info = model.load_model(sess=sess, fname=os.path.join(log_path, 'last_model.pkl'))
        rl_predict_valid, rl_perf_valid = predict(sess, model, data=valid_data, model_info=model_info,
                                                  set_var=set_var)
        # FF: True Positive, TT: True Negative, TF: False Negative, FT: False Positive
        FF = rl_perf_valid["for_FF"]
        TT = rl_perf_valid["for_TT"]
        TF = rl_perf_valid["for_TF"]
        FT = rl_perf_valid["for_FT"]
        reward = (sum(rl_predict_valid["rewards"])) / len(rl_predict_valid["rewards"])
        # FF: True Positive, TT: True Negative, TF: False Negative, FT: False Positive
        accuracy = (FF + TT) / (FF + TT + TF + FT)
        precision = FF / (FF + FT)
        recall = FF / (FF + TF)
        f1 = 2 * FF / (2 * FF + FT + TF)
        print(
            "Accuracy: {:02.5f} | Precision: {:02.5f} | Recall: {:02.5f} | F1 score: {:02.5f} | Reward: {:02.5f}".format(
                accuracy, precision, recall, f1, reward))
        ########################################################################################################################
        # Area Under the Precision-Recall Curve (AUPRC). Recommened by https://www.kaggle.com/mlg-ulb/creditcardfraud
        ########################################################################################################################
        y_labels = rl_predict_valid[model_info["y_var"][0]].astype(int).values
        y_scores = rl_predict_valid["rl_p1"].values
        precisions, recalls, _thresholds = precision_recall_curve(y_labels, y_scores)
        auprc_fig = plt.figure(figsize=(10, 10))
        plt.plot([0, 1], [0, 1], 'k--')
        plt.plot(precisions, recalls, label='old-model')
        plt.xlabel('Precision')
        plt.ylabel('Recall')
        plt.title('Precision-Recall Curve')
        auprc_fig.savefig(log_path + "/AUPRC" + ".png")
        ########################################################################################################################
        # ROC
        ########################################################################################################################
        fpr_rf_dt, tpr_rf_dt, _ = roc_curve(y_labels, y_scores)
        auprc_fig = plt.figure(figsize=(10, 10))
        plt.plot([0, 1], [0, 1], 'k--')
        plt.plot(fpr_rf_dt, tpr_rf_dt, label='old-model')
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC Curve')
        auprc_fig.savefig(log_path + "/ROC" + ".png")

    sess.__exit__(None, None, None)