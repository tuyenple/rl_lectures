import gym
from gym.wrappers import Monitor
import numpy as np
import os
import random
import sys
import tensorflow as tf
from collections import deque, namedtuple
import pickle
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="6"

class StateProcessor():
    """
    Processes a raw Atari images. Resizes it and converts it to grayscale.
    """
    def __init__(self):
        # Build the Tensorflow graph
        with tf.variable_scope("state_processor"):
            self.input_state = tf.placeholder(shape=[210, 160, 3], dtype=tf.uint8)
            self.output = tf.image.rgb_to_grayscale(self.input_state)
            self.output = tf.image.crop_to_bounding_box(self.output, 34, 0, 160, 160)
            self.output = tf.image.resize_images(
                self.output, [84, 84], method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
            self.output = tf.squeeze(self.output)

    def process(self, sess, state):
        """
        Args:
            sess: A Tensorflow session object
            state: A [210, 160, 3] Atari RGB State

        Returns:
            A scale image [84, 84]
        """
        return sess.run(self.output, { self.input_state: state })

class QNetwork():
    def __init__(self, nActions, scope="network"):
        self.scope = scope
        self.nActions = nActions
        with tf.variable_scope(scope):
            self._build_model()

    def _build_model(self):
        # Input are RGB frames of shape 84, 84 each
        self.obs_n = tf.placeholder(shape=[None, 84, 84, 4], dtype=tf.uint8, name="obs")
        # Scale
        inputscaled = tf.to_float(self.obs_n) / 255.0

        # Three convolutional layers
        conv1 = tf.layers.conv2d(inputscaled, 32, [8, 8], 4, "valid",
                                 kernel_initializer=tf.variance_scaling_initializer(seed=123),
                                 activation=tf.nn.relu,
                                 use_bias=False,
                                 name='conv1')
        conv2 = tf.layers.conv2d(conv1, 64, [4, 4], 2, "valid",
                                 kernel_initializer=tf.variance_scaling_initializer(seed=123),
                                 activation=tf.nn.relu,
                                 use_bias=False,
                                 name='conv2')
        conv3 = tf.layers.conv2d(conv2, 64, [3, 3], 1, "valid",
                                 kernel_initializer=tf.variance_scaling_initializer(seed=123),
                                 activation=tf.nn.relu,
                                 use_bias=False,
                                 name='conv3')

        # Fully connected layers
        flattened = tf.layers.flatten(conv3)
        fc1 = tf.layers.dense(flattened, 512,
                              kernel_initializer=tf.variance_scaling_initializer(seed=123),
                              activation=tf.nn.relu,
                              use_bias=False,
                              name='fc1')
        self.q_values = tf.layers.dense(fc1, self.nActions,
                                        kernel_initializer=tf.variance_scaling_initializer(seed=123),
                                        use_bias=False,
                                        name='q_values')

        # y = r + gamma*max Q', calculated in the main function
        self.y_n = tf.placeholder(shape=[None], dtype=tf.float32, name="target")
        # Action that was performed
        self.act_n = tf.placeholder(shape=[None], dtype=tf.int32, name="actions")
        # Q value of the action that was performed
        action_one_hot = tf.one_hot(self.act_n, self.nActions, dtype=tf.float32)
        Q = tf.reduce_sum(self.q_values * action_one_hot, axis=1)

        # Calculate the loss
        # self.losses = tf.squared_difference(self.y_n, Q)
        self.losses = tf.losses.huber_loss(self.y_n, Q)
        self.loss = tf.reduce_mean(self.losses)

        # Optimizer Parameters from original paper
        # self.optimizer = tf.train.RMSPropOptimizer(learning_rate=0.00025)
        self.optimizer = tf.train.AdamOptimizer(learning_rate=0.00025)
        self.train_op = self.optimizer.minimize(self.loss)

    def predict(self, sess, s):
        """
        Predicts action values.

        Args:
          sess: Tensorflow session
          s: State input of shape [batch_size, 84, 84, 4]

        Returns:
          Tensor of shape [batch_size, NUM_VALID_ACTIONS] containing the estimated
          action values.
        """
        return sess.run(self.q_values, { self.obs_n: s })

    def update(self, sess, s, a, y):
        """
        Updates the estimator towards the given targets.

        Args:
          sess: Tensorflow session object
          s: State input of shape [batch_size, 4, 84, 84, 3]
          a: Chosen actions of shape [batch_size]
          y: Targets of shape [batch_size]

        Returns:
          The calculated loss on the batch.
        """
        feed_dict = { self.obs_n: s, self.y_n: y, self.act_n: a }
        _, loss = sess.run([self.train_op, self.loss], feed_dict)
        return loss

class TargetNetworkUpdater:
    """Copies the parameters of the main DQN to the target DQN"""

    def __init__(self, main_dqn_vars, target_dqn_vars, tau):
        """
        Args:
            main_dqn_vars: A list of tensorflow variables belonging to the main DQN network
            target_dqn_vars: A list of tensorflow variables belonging to the target DQN network
        """
        self.main_dqn_vars = main_dqn_vars
        self.target_dqn_vars = target_dqn_vars
        self.tau = tau
        self.update_ops = None

    def _hard_update_target_vars(self):
        update_ops = []
        for i, var in enumerate(self.main_dqn_vars):
            copy_op = self.target_dqn_vars[i].assign((1-self.tau) *var.value() + self.tau*self.target_dqn_vars[i].value())
            update_ops.append(copy_op)
        return update_ops

    def _soft_update_target_vars(self):
        if (self.update_ops == None):
            self.update_ops = []
            for i, var in enumerate(self.main_dqn_vars):
                copy_op = self.target_dqn_vars[i].assign((1-self.tau) *var.value() + self.tau*self.target_dqn_vars[i].value())
                self.update_ops.append(copy_op)
        return self.update_ops

    def update_networks(self, sess, is_first=False):
        """
        Args:
            sess: A Tensorflow session object
        Assigns the values of the parameters of the main network to the
        parameters of the target network
        """
        if (is_first):
            update_ops = self._hard_update_target_vars()
        else:
            update_ops = self._soft_update_target_vars()

        for copy_op in update_ops:
            sess.run(copy_op)

def greedy_policy(sess, network, obs, epsilon, nA, warmup=False):
    """
    Creates an epsilon-greedy policy based on a given Q-network and epsilon.

    Args:
        sess: tf session
        network: An network that returns q values for a given state
        nA: Number of actions in the environment.
        epsilon: t

    Returns:
        An action
    """
    if (random.random() < epsilon or warmup==True):
        action = random.randint(0, nA-1)
    else:
        q_values = network.predict(sess, np.expand_dims(obs, 0))[0]
        action = np.argmax(q_values)
    return action

def main():
    # Hyperparameters
    num_episodes=5000
    replay_memory_size=100000
    replay_memory_init_size=1000
    epsilon_start=1.0
    epsilon_end=0.1
    epsilon_decay_steps=1000000
    discount_factor=0.99
    batch_size=32
    update_frequence=4
    tau=0.999

    env = gym.envs.make("PongDeterministic-v4")
    # Save path
    experiment_dir = os.path.abspath("./experiments/{}".format(env.spec.id))

    # Record videos
    env = Monitor(env,
                  directory=os.path.join(experiment_dir, "monitor"),
                  resume=True,
                  force=True)

    tf.reset_default_graph()
    # Create networks
    q_network = QNetwork(nActions=env.action_space.n, scope="q")
    target_q_network = QNetwork(nActions=env.action_space.n, scope="target_q")
    MAIN_DQN_VARS = tf.trainable_variables(scope='q')
    TARGET_DQN_VARS = tf.trainable_variables(scope='target_q')
    targetNetworkUpdater = TargetNetworkUpdater(MAIN_DQN_VARS, TARGET_DQN_VARS, tau)
    # State processor
    state_processor = StateProcessor()

    sess = tf.Session()
    sess.__enter__()  # equivalent to `with sess:`
    sess.run(tf.global_variables_initializer())
    targetNetworkUpdater.update_networks(sess,is_first=True)

    Transition = namedtuple("Samples", ["state", "action", "reward", "next_state", "done"])

    # The replay memory
    replay_memory = deque(maxlen=replay_memory_size)

    # The epsilon decay schedule
    epsilons = np.linspace(epsilon_start, epsilon_end, epsilon_decay_steps)
    average_reward = deque(maxlen=1000)
    episode_rewards = []
    episode_lengths = []
    episode_reward = 0
    total_t = 0
    t = 0
    loss = None
    for i_episode in range(num_episodes):

        # Reset the environment
        state = env.reset()
        state = state_processor.process(sess, state)
        state = np.stack([state] * 4, axis=2)

        # Run environment
        while True:

            # Epsilon for this time step
            epsilon = epsilons[min(total_t, epsilon_decay_steps - 1)]

            # Take a step
            warmup = total_t < replay_memory_init_size
            action = greedy_policy(sess, q_network, state, epsilon, env.action_space.n, warmup=warmup)

            # Get next state
            next_state, reward, done, _ = env.step(action)
            next_state = state_processor.process(sess, next_state)
            next_state = np.append(state[:, :, 1:], np.expand_dims(next_state, 2), axis=2)

            # Save transition to replay memory
            replay_memory.append(Transition(state, action, reward, next_state, 0 if done is True else 1))

            if (not warmup and total_t%update_frequence==0):
                # Sample a minibatch from the replay memory
                samples = random.sample(replay_memory, batch_size)
                states_batch, action_batch, reward_batch, next_states_batch, done_batch = map(np.array, zip(*samples))

#                 # DQN
#                 q_values_next = target_q_network.predict(sess, next_states_batch)
#                 max_q = np.max(q_values_next, axis=1)
#                 targets_batch = reward_batch + discount_factor * done_batch * max_q
                
                # Double DQN
                q_values_next = target_q_network.predict(sess, next_states_batch)
                arg_q_max = np.argmax(q_network.predict(sess, next_states_batch), axis=1)
                double_q = q_values_next[range(batch_size), arg_q_max]
                targets_batch = reward_batch + discount_factor * done_batch * double_q

                # Perform gradient descent update
                loss = q_network.update(sess, states_batch, action_batch, targets_batch)

            # Update statistics
            state = next_state
            t += 1
            total_t += 1
            episode_reward += reward

            targetNetworkUpdater.update_networks(sess)

            if done:
                # Update statistics
                episode_rewards.append(episode_reward)
                average_reward.append(episode_reward)
                episode_lengths.append(t)

                # Print out
                print("Episode {}/{}, total steps: {}, reward (last 100 episode): {}, loss: {}".format(i_episode + 1, num_episodes,
                                                                                    total_t, np.mean(average_reward), loss))
                sys.stdout.flush()

                # Reset variable
                t = 0
                episode_reward = 0
                break

        if ((i_episode+1)%100==0):
            with open(os.path.join(experiment_dir, 'rewards.pkl'), 'wb') as f:
                pickle.dump(episode_rewards, f, pickle.HIGHEST_PROTOCOL)
            with open(os.path.join(experiment_dir, 'episode_lengths.pkl'), 'wb') as f:
                pickle.dump(episode_lengths, f, pickle.HIGHEST_PROTOCOL)

    env.monitor.close()

if __name__ == "__main__":
  main()