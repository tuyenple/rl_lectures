import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import os
experiment_dir = os.path.abspath("./experiments/{}".format("PongDeterministic-v4"))
with open(os.path.join(experiment_dir, 'rewards.pkl'), "rb") as file:
    rewards = pickle.load(file)

plt.figure()
plt.plot(rewards)
plt.show()