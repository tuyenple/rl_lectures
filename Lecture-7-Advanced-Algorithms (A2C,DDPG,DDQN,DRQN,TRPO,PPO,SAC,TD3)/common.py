import tensorflow as tf
import numpy as np
import scipy.signal
import pickle
import os
import shutil

eps = 1e-8

def flat_concat(xs):
    return tf.concat([tf.reshape(x,(-1,)) for x in xs], axis=0)

def flat_grad(f, params):
    return flat_concat(tf.gradients(xs=params, ys=f))

def find_trainable_variables(key):
    """
    Returns the trainable variables within a given scope

    :param key: (str) The variable scope
    :return: ([TensorFlow Tensor]) the trainable variables
    """
    with tf.variable_scope(key):
        return tf.trainable_variables()

def get_vars(scope=''):
    return [x for x in tf.trainable_variables() if scope in x.name]

def count_vars(scope=''):
    v = get_vars(scope)
    return sum([np.prod(var.shape.as_list()) for var in v])

def assign_params_from_flat(x, params):
    flat_size = lambda p : int(np.prod(p.shape.as_list())) # the 'int' is important for scalars
    splits = tf.split(x, [flat_size(p) for p in params])
    new_params = [tf.reshape(p_new, p.shape) for p, p_new in zip(params, splits)]
    return tf.group([tf.assign(p, p_new) for p, p_new in zip(params, new_params)])

def discount_cumsum(x, discount):
    """
    magic from rllab for computing discounted cumulative sums of vectors.

    input:
        vector x,
        [x0,
         x1,
         x2]

    output:
        [x0 + discount * x1 + discount^2 * x2,
         x1 + discount * x2,
         x2]
    """
    return scipy.signal.lfilter([1], [1, float(-discount)], x[::-1], axis=0)[::-1]

def mlp(input_placeholder, output_size, hidden_layers, hidden_activation=tf.tanh, output_activation=None, use_bias=True, initializer=None):
    out = input_placeholder
    for unit in hidden_layers:
        out = tf.layers.dense(inputs=out, units=unit, activation=hidden_activation, use_bias=use_bias, kernel_initializer=initializer)

    out = tf.layers.dense(inputs=out, units=output_size, activation=output_activation, use_bias=use_bias, kernel_initializer=initializer)
    return out

def mlp_finance(input_placeholder, output_size, hidden_layers, hidden_activation=tf.tanh, output_activation=None, use_bias=True, initializer=None):
    out = input_placeholder
    for unit in hidden_layers:
        out = tf.layers.dense(inputs=out, units=unit, activation=hidden_activation, use_bias=use_bias, kernel_initializer=initializer)

    out = tf.layers.dense(inputs=out, units=output_size, activation=output_activation, use_bias=use_bias, kernel_initializer=initializer)
    return out

def santander(input_placeholder, output_size, hidden_layers, hidden_activation=tf.tanh, output_activation=None, use_bias=True, initializer=None):
    out = input_placeholder
    for unit in hidden_layers:
        out = tf.layers.dense(inputs=out, units=unit, activation=hidden_activation, use_bias=use_bias, kernel_initializer=initializer)
        out = tf.layers.dropout(out)

    out = tf.layers.dense(inputs=out, units=output_size, activation=output_activation, use_bias=use_bias, kernel_initializer=initializer)
    return out

LOG_STD_MAX = 2
LOG_STD_MIN = -20

def clip_but_pass_gradient(x, l=-1., u=1.):
    clip_up = tf.cast(x > u, tf.float32)
    clip_low = tf.cast(x < l, tf.float32)
    return x + tf.stop_gradient((u - x)*clip_up + (l - x)*clip_low)

def apply_squashing_func(mu, pi, logp_pi):
    mu = tf.tanh(mu)
    pi = tf.tanh(pi)
    # To avoid evil machine precision error, strictly clip 1-pi**2 to [0,1] range.
    logp_pi -= tf.reduce_sum(tf.log(clip_but_pass_gradient(1 - pi**2, l=0, u=1) + 1e-6), axis=1)
    return mu, pi, logp_pi

def sac_mlp(state_placeholder, action_placeholder, hidden_layers, hidden_activation=tf.nn.tanh, output_activation=tf.nn.tanh):
    act_dim = action_placeholder.shape.as_list()[-1]
    # policy
    with tf.variable_scope('pi'):
        net = state_placeholder
        for unit in hidden_layers:
            net = tf.layers.dense(inputs=net, units=unit, activation=hidden_activation)

        mu = tf.layers.dense(net, act_dim, activation=output_activation)

        log_std = tf.layers.dense(net, act_dim, activation=tf.nn.tanh)
        log_std = LOG_STD_MIN + 0.5 * (LOG_STD_MAX - LOG_STD_MIN) * (log_std + 1)

        std = tf.exp(log_std)
        pi = mu + tf.random_normal(tf.shape(mu)) * std
        logp_pi = gaussian_likelihood(pi, mu, log_std)

        mu, pi, logp_pi = apply_squashing_func(mu, pi, logp_pi)

    action_scale = 1.0
    mu *= action_scale
    pi *= action_scale

    # vfs
    vf_mlp = lambda x: tf.squeeze(mlp(x, 1, hidden_layers, hidden_activation, None), axis=1)
    with tf.variable_scope('q1'):
        q1 = vf_mlp(tf.concat([state_placeholder, action_placeholder], axis=-1))
    with tf.variable_scope('q1', reuse=True):
        q1_pi = vf_mlp(tf.concat([state_placeholder, pi], axis=-1))
    with tf.variable_scope('q2'):
        q2 = vf_mlp(tf.concat([state_placeholder, action_placeholder], axis=-1))
    with tf.variable_scope('q2', reuse=True):
        q2_pi = vf_mlp(tf.concat([state_placeholder, pi], axis=-1))
    with tf.variable_scope('v'):
        v = vf_mlp(state_placeholder)

    return mu, pi, logp_pi, q1, q2, q1_pi, q2_pi, v

def adversary_mlp(obs_ph, acs_ph, hidden_layers, hidden_activation=tf.nn.relu, output_activation=None):
    out = tf.concat([obs_ph, acs_ph], axis=1)
    for unit in hidden_layers:
        out = tf.layers.dense(inputs=out, units=unit, activation=hidden_activation)

    out = tf.layers.dense(inputs=out, units=1, activation=output_activation)
    return out

def gaussian_likelihood(x, mu, log_std):
    pre_sum = -0.5 * (((x-mu)/(tf.exp(log_std)+eps))**2 + 2*log_std + np.log(2*np.pi))
    return tf.reduce_sum(pre_sum, axis=1)

def categorical_kl(logp0, logp1):
    """
    tf symbol for mean KL divergence between two batches of categorical probability distributions,
    where the distributions are input as log probs.
    """
    all_kls = tf.reduce_sum(tf.exp(logp1) * (logp1 - logp0), axis=1)
    return tf.reduce_mean(all_kls)

def diagonal_gaussian_kl(mu0, log_std0, mu1, log_std1):
    """
    tf symbol for mean KL divergence between two batches of diagonal gaussian distributions,
    where distributions are specified by means and log stds.
    (https://en.wikipedia.org/wiki/Kullback-Leibler_divergence#Multivariate_normal_distributions)
    """
    var0, var1 = tf.exp(2 * log_std0), tf.exp(2 * log_std1)
    pre_sum = 0.5*(((mu1- mu0)**2 + var0)/(var1 + eps) - 1) +  log_std1 - log_std0
    all_kls = tf.reduce_sum(pre_sum, axis=1)
    return tf.reduce_mean(all_kls)

def save_model(fpath, tf_saver_elements, tf_saver_info, log_save_name="last_model"):
    """
    Uses simple_save to save a trained model, plus info to make it easy
    to associated tensors to variables after restore.
    """
    if not os.path.exists(fpath):
        os.makedirs(fpath, exist_ok=True, mode=0o777)
    if os.path.exists(os.path.join(fpath, log_save_name)):
        shutil.rmtree(os.path.join(fpath, log_save_name))

    tf.saved_model.simple_save(export_dir=os.path.join(fpath, log_save_name), **tf_saver_elements)

    with open(os.path.join(fpath, 'model_info.pkl'), "wb") as file:
        pickle.dump(tf_saver_info, file)

def load_model(sess, fpath, log_save_name="last_model"):
    tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING], os.path.join(fpath, log_save_name))
    with open(os.path.join(fpath, 'model_info.pkl'), "rb") as file:
        model_info = pickle.load(file)

    graph = tf.get_default_graph()
    model = dict()
    model.update({k: graph.get_tensor_by_name(v) for k,v in model_info['inputs'].items()})
    model.update({k: graph.get_tensor_by_name(v) for k,v in model_info['outputs'].items()})
    return model